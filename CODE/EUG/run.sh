# run DukeMTMC-VideoReID
python3 run.py --dataset DukeMTMC-VideoReID --logs_dir logs/DukeMTMC-VideoReID_EF_10/ --EF 10 --mode Dissimilarity --max_frames 900

# run mars
#python3 run.py --dataset mars --logs_dir logs/mars_EF_10/ --EF 10 --mode Dissimilarity --max_frames 900

# if you need to resume 
# python3 run.py --dataset mars --logs_dir logs/mars_EF_10/ --EF 10 --mode Dissimilarity --max_frames 900 --resume logs/mars_EF_10/ 

